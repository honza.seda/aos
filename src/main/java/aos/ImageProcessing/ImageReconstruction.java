package aos.ImageProcessing;

import aos.Controller.MainController;

import java.awt.image.WritableRaster;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Class implementing Algebraic Reconstruction Techniques - Additive and Multiplicative
 */
public class ImageReconstruction {
    private ImageHandler originalImage;
    private ImageHandler estimation;
    private int projectionsCount;
    private int iterationsCount;
    private HashSet<Integer> projectionAngles;

    private MainController mainController;
    private boolean animationEnabled = false;
    private boolean checkError = false;

    private double lastIterationEstimationError = 0;
    private int lowestEstimationErrorIndex = 0;

    public ImageReconstruction(ImageHandler imageHandler, int projectionsCount, int iterationsCount, MainController mainController) {
        this.originalImage = imageHandler;
        this.projectionsCount = projectionsCount;
        this.mainController = mainController;
        this.iterationsCount = iterationsCount;
        setInitialEstimation();
        setProjectionAngles();
    }

    public int getAngleIncrement(){
        return projectionsCount < 4 ? 180/projectionsCount : 360/projectionsCount;
    }

    private void setProjectionAngles()
    {
        projectionAngles = new HashSet<>();
        for (int i = 0; i < projectionsCount; i++)
        {
            projectionAngles.add((i * getAngleIncrement())%90);
        }
    }

    /**
     * Creates the initial estimation e_0 from original image
     */
    private void setInitialEstimation() {
        double[] sumInProjection = originalImage.getSumForProjection();
        double sum = 0;

        for (double i : sumInProjection) {
            sum += i;
        }
        double average = sum / (sumInProjection.length * sumInProjection.length);
        double[][] matrix = new double[originalImage.getImage().getHeight()][originalImage.getImage().getWidth()];

        for (double[] row: matrix) {
            Arrays.fill(row, average);
        }
        this.estimation = new ImageHandler(matrix, true);
    }

    /**
     * Algorithm for additive ART reconstruction of the original image
     */
    public void artReconstruction() {
        long startTime = System.currentTimeMillis();
        if(originalImage.getSinogram() == null)
        {
            originalImage.generateSinogram(mainController);
        }
        mainController.updateStatusText("Probíhá výpočet rekonstrukce obrazu aditivní metodou.");
        lastIterationEstimationError = 0;
        lowestEstimationErrorIndex = 0;

        for (int i = 0; i < iterationsCount; i++) {
            if (Thread.interrupted()) {
                return;
            }
            int angle = i%projectionsCount * getAngleIncrement();
            double[] r_i = originalImage.getSinogram()[angle];
            estimation.rotateImage(angle);
            double[] s_est = estimation.getSumForProjection();
            double[] sumDifference = new double[r_i.length];
            for (int j = 0; j < sumDifference.length; j++) {
                sumDifference[j] = (r_i[j] - s_est[j]) / (double) r_i.length;
            }

            double[][] nextIterationEstimation = estimation.getOriginalImageAsArray();
            for (int x = 0; x < estimation.getImage().getWidth(); x++) {
                for (int y = 0; y < estimation.getImage().getHeight(); y++) {
                    double newEstimation = nextIterationEstimation[x][y] + sumDifference[x];
                    if(newEstimation < 0) {
                        newEstimation = 0;
                    }
                    if(newEstimation > 255) {
                        newEstimation = 255;
                    }
                    nextIterationEstimation[x][y] = newEstimation;
                }
            }

            estimation = new ImageHandler(nextIterationEstimation, false);
            estimation.rotateImage(-angle);
            mainController.updateProgressBar(i/(double)iterationsCount,true);
            outputIteration();

            if(checkError) {
                if(isEstimationErrorZero(i, startTime)) {
                    return;
                }
            }
            else
            {
                if ((i+1) % projectionsCount == 0) {
                    if(isEstimationErrorZero(i, startTime)){
                        return;
                    }
                }
            }
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        mainController.updateStatusText(String.format("Rekonstrukce dokončena ("+ elapsedTime +"ms ). Ukončení maximálním počtem iterací. Chyba estimace: %.3f ("+lowestEstimationErrorIndex+". iterace)", lastIterationEstimationError));
        mainController.updateProgressBar(0,false);
        mainController.updateEstimatedImage(estimation.getImage());

    }

    /**
     * Algorithm for multiplicative ART reconstruction of the original image
     */
    public void mrtReconstruction() {
        long startTime = System.currentTimeMillis();
        if(originalImage.getSinogram() == null)
        {
            originalImage.generateSinogram(mainController);
        }
        mainController.updateStatusText("Probíhá výpočet rekonstrukce obrazu multiplikativní metodou.");
        lastIterationEstimationError = 0;
        lowestEstimationErrorIndex = 0;

        for (int i = 0; i < iterationsCount; i++) {
            if (Thread.interrupted()) {
                return;
            }
            int angle = i%projectionsCount * getAngleIncrement();
            double[] r_i = originalImage.getSinogram()[angle];
            estimation.rotateImage(angle);
            double[] s_est = estimation.getSumForProjection();
            double[] multiplier = new double[r_i.length];
            for (int j = 0; j < multiplier.length; j++) {
                multiplier[j] = r_i[j] / (float) s_est[j];
            }

            double[][] nextIterationEstimation = estimation.getOriginalImageAsArray();
            for (int x = 0; x < estimation.getImage().getWidth(); x++) {
                for (int y = 0; y < estimation.getImage().getHeight(); y++) {
                    double newEstimation = nextIterationEstimation[x][y] * multiplier[x];
                    if(newEstimation < 0) {
                        newEstimation = 0;
                    }
                    if(newEstimation > 255) {
                        newEstimation = 255;
                    }
                    nextIterationEstimation[x][y] = newEstimation;
                }
            }

            estimation = new ImageHandler(nextIterationEstimation, false);
            estimation.rotateImage(-angle);
            mainController.updateProgressBar(i/(double)iterationsCount,true);
            outputIteration();

            if(checkError) {
                if(isEstimationErrorZero(i, startTime)) {
                    return;
                }
            }
            else
            {
                if ((i+1) % projectionsCount == 0) {
                    if(isEstimationErrorZero(i, startTime)){
                        return;
                    }
                }
            }

        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        mainController.updateStatusText(String.format("Rekonstrukce dokončena ("+ elapsedTime + "ms). Ukončení maximálním počtem iterací. Chyba estimace: %.3f ("+lowestEstimationErrorIndex+". iterace)", lastIterationEstimationError));
        mainController.updateProgressBar(0,false);
        mainController.updateEstimatedImage(estimation.getImage());
    }

    /**
     * Calculates the gray level variance for current estimation iteration as a stop condition for algorithm iteration
     *
     * @param angle current iteration angle
     * @return
     */
    private long calculateGrayLevelVariance(int angle) {
        double mean = 0;
        int n = originalImage.getImage().getWidth();
        long v = 0;
        double[] rgba = new double[4];
        WritableRaster raster = estimation.getImage().getRaster();

        for (int i = 0; i < n; i++)
        {
            mean += originalImage.getSinogram()[angle][i]/(n);
        }
        for (int x = 0; x < raster.getWidth(); x++) {
            for (int y = 0; y < raster.getHeight(); y++) {
                raster.getPixel(x,y,rgba);
                double pixelColor = (rgba[0] + rgba[1] + rgba[2]) / 3.0;
                v += Math.pow(pixelColor-mean,2);
            }
        }

        return v/(n*n);
    }

    private boolean isEstimationErrorZero(int iteration, long startTime)
    {
       double sum = 0;

       double[][] estimationProjections = estimation.getProjectionsForAllAngles(projectionAngles);
       int n = estimation.getImage().getWidth();
       for (int i = 0; i < projectionsCount; i++)
       {
           int angle = i * getAngleIncrement();
           for (int j = 0; j < n; j++)
           {
               sum += Math.abs(originalImage.getSinogram()[angle][j] - estimationProjections[angle][j]);
           }
       }
       double error = sum / (projectionsCount * n * n);
       if (error > 0){
           if(lowestEstimationErrorIndex == 0) {
               lastIterationEstimationError = error;
               lowestEstimationErrorIndex = iteration+1;
           }
           if (error < lastIterationEstimationError) {
               lastIterationEstimationError = error;
               lowestEstimationErrorIndex = iteration+1;
           }
           return false;
       }
       else
       {
           long stopTime = System.currentTimeMillis();
           long elapsedTime = stopTime - startTime;
           int endIteration = iteration+1;
           mainController.updateStatusText("Rekonstrukce dokončena (" + elapsedTime + "ms). Ukončeno dosažením nulové chyby estimace v " + endIteration + ". iteraci");
           mainController.updateProgressBar(0, false);
           mainController.updateEstimatedImage(estimation.getImage());
           return true;
       }
    }

    /**
     * Updates the estimation image in ui from current estimation
     */
    private void outputIteration() {
        if(animationEnabled) {
            mainController.updateEstimatedImage(estimation.getImage());

            try {
                Thread.sleep(200/getProjectionsCount());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public ImageHandler getOriginalImage() {
        return originalImage;
    }

    public int getProjectionsCount() {
        return projectionsCount;
    }

    public void setProjectionsCount(int projectionsCount) {
        this.projectionsCount = projectionsCount;
    }

    public ImageHandler getEstimation() {
        return estimation;
    }

    public int getIterationsCount() {
        return iterationsCount;
    }

    public void setAnimationEnabled(boolean animationEnabled) {
        this.animationEnabled = animationEnabled;
    }

    public boolean isCheckError() {
        return checkError;
    }

    public void setCheckError(boolean checkError) {
        this.checkError = checkError;
    }
}
