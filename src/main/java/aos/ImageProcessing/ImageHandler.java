package aos.ImageProcessing;

import aos.Controller.MainController;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ImageHandler {
    private BufferedImage image;
    private double[][] sinogram;
    private double[][] nonNullPixelCount;

    /**
     * Constructor that creates handler from BufferedImage
     *
     * @param image Input image
     */
    public ImageHandler(BufferedImage image) {
        this.image = getGrayScaleImage(image);
//        setImagePadding();
    }

    /**
     * Constructor that creates handler from array of pixels
     *
     * @param pixelData array of pixels
     * @param initialize
     */
    public ImageHandler(double[][] pixelData, boolean initialize) {
        setOriginalImageFromArray(pixelData);
    }

    public BufferedImage getImage() {
        return image;
    }

    public double[][] getSinogram() {
        return sinogram;
    }

    /**
     * Converts passed image to grayscale
     *
     * @param image
     * @return BufferedImage
     */
    private static BufferedImage getGrayScaleImage(BufferedImage image) {
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                Color pixelColor = new Color(image.getRGB(x, y));
                int r = pixelColor.getRed();
                int g = pixelColor.getGreen();
                int b = pixelColor.getBlue();
                int grayLevel = (r + g + b) / 3;
                Color grayPixel = new Color(grayLevel, grayLevel, grayLevel);
                image.setRGB(x, y, grayPixel.getRGB());
            }
        }
        return image;
    }

    /**
     * Returns image as 2D array
     *
     * @return int[][]
     */
    public double[][] getOriginalImageAsArray() {
        int imageWidth = this.image.getWidth();
        int imageHeight = this.image.getHeight();
        double[][] pixels = new double[imageWidth][imageHeight];

        for (int x = 0; x < imageWidth; x++) {
            for (int y = 0; y < imageHeight; y++) {
                Color pixelColor = new Color(this.image.getRGB(x, y));
                int r = pixelColor.getRed();
                int g = pixelColor.getGreen();
                int b = pixelColor.getBlue();
                pixels[x][y] = (r + g + b) / 3.0;
            }
        }

        return pixels;
    }

    /**
     * Rotates image by an angle in degrees.
     * The computational time depends heavily on used AffineTransformation interpolation, with bicubic being the slowest
     * but more precise, nearest neighbours fastest but with poor interpolation results and bilinear in between them.
     *
     * @param angle in degrees (radian)
     */
    public void rotateImage(int angle)
    {
        this.image = getRotatedImage(angle, AffineTransformOp.TYPE_BICUBIC);
    }

    /**
     * Returns BufferedImage of image rotated by specified angle
     *
     * @param angle in degrees
     * @param interpolationType specifies the interpolation type for AffineTransofrm rotation
     * @return rotated BufferedImage
     */
    private BufferedImage getRotatedImage(int angle, int interpolationType)
    {
        double rotationRequired = Math.toRadians (angle);
        double locationX = (double)this.image.getWidth() / 2;
        double locationY = (double)this.image.getHeight() / 2;
        AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
        AffineTransformOp op = new AffineTransformOp(tx, interpolationType);
        BufferedImage rotatedImage =new BufferedImage(this.image.getWidth(), this.image.getHeight(), this.image.getType());
        op.filter(this.image, rotatedImage);

        return rotatedImage;
    }

    /**
     * returns array of sums of pixel colors for all columns in a single image projection
     *
     * @return double[]
     */
    public double[] getSumForProjection()
    {
        WritableRaster raster = image.getRaster();
        double[] brightnessSum = new double[raster.getWidth()];
        double[] rgba = new double[4];

        for (int x = 0; x < raster.getWidth(); x++) {
            int colSum = 0;
            for (int y = 0; y < raster.getHeight(); y++) {
                raster.getPixel(x,y,rgba);
                colSum += (rgba[0] + rgba[1] + rgba[2]) / 3.0;
            }
            brightnessSum[x] = colSum;
        }
        return brightnessSum;
    }

    /**
     * Sets BufferedImage image from two dimensional array, array coordinates [x][y]
     *
     * @param pixelData array of pixels
     */
    public void setOriginalImageFromArray(double[][] pixelData) {
        BufferedImage bi = new BufferedImage( pixelData.length, pixelData[0].length, BufferedImage.TYPE_INT_RGB );
        for(int y=0;y<pixelData.length;y++){
            for(int x=0;x<pixelData[y].length;x++){
                int Pixel=(int)pixelData[x][y]<<16 | (int)pixelData[x][y] << 8 | (int)pixelData[x][y];
                bi.setRGB(x, y,Pixel);
            }

        }
        this.image = bi;
    }

    /**
     * Inserts black border (of size maxsize - actualsize) around image so that no pixels are lost when rotating image
     */
    private void setImagePadding() {
        int[] maxSize = getMaxSize(image);

        BufferedImage result = new BufferedImage(maxSize[0], maxSize[1], image.getType());
        Graphics2D graphics2D = result.createGraphics();
        graphics2D.drawImage(image, (maxSize[0] / 2) - (image.getWidth() / 2), (maxSize[1] / 2) - (image.getHeight() / 2), null);
        graphics2D.dispose();

        this.image = result;
    }

    /**
     * Returns max size which image could be when rotating (at 45 degrees)
     *
     * @param image : Image
     * @return int[]
     */
    private int[] getMaxSize(BufferedImage image)
    {
        double rads = Math.toRadians(45);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = image.getWidth();
        int h = image.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        return new int[]{newWidth, newHeight};
    }

    /**
     * Computes sums of projections for all possible angles
     *
     * @param mainController controller class (for updating progress in ui)
     */
    public void generateSinogram(MainController mainController) {
        double[][] sinogram = new double[360][this.image.getWidth()];
        mainController.updateStatusText("Vytváření projekcí pro sinogram.");
        for (int i = 0; i < 90; i++) {
            if (Thread.interrupted()) {
                return;
            }
            mainController.updateProgressBar(i/(double)90, true);
            sinogram = getProjectionsForRotation(i, sinogram);
        }
        this.sinogram = sinogram;
        mainController.updateProgressBar(0, false);
    }

    public double[][] getProjectionsForAllAngles(HashSet<Integer> angles) {
        double[][] sinogram = new double[360][this.image.getWidth()];
        List<Integer> computedProjections = new ArrayList<>();
        for (int angle : angles) {
            if(!computedProjections.contains(angle)) {
                sinogram = getProjectionsForRotation(angle, sinogram);
                computedProjections.addAll(Arrays.asList(angle, angle + 90, angle + 180, angle + 270));
            }
        }
        return sinogram;
    }

    private double[][] getProjectionsForRotation(int angle, double[][] sinogram) {

        WritableRaster raster = getRotatedImage(angle, AffineTransformOp.TYPE_BICUBIC).getRaster();
        double[] brightnessSum = new double[raster.getWidth()];
        double[] rgba = new double[4];

        for (int x = 0; x < raster.getWidth(); x++) {
            int colSum = 0;
            for (int y = 0; y < raster.getHeight(); y++) {
                raster.getPixel(x,y,rgba);
                double pixelColor = (rgba[0] + rgba[1] + rgba[2]) / 3.0;
                colSum += pixelColor;
            }
            brightnessSum[x] = colSum;
        }
        sinogram[angle] = brightnessSum;

        double[] brightnessSumMirrored = new double[raster.getWidth()];
        for(int j = 0; j < brightnessSumMirrored.length; j++)
        {
            brightnessSumMirrored[j] = brightnessSum[brightnessSum.length - j - 1];
        }
        sinogram[angle+180] = brightnessSumMirrored;

        double[] brightnessSumPerpendicular = new double[raster.getWidth()];

        for (int y = 0; y < raster.getHeight(); y++) {
            int rowSum = 0;
            for (int x = 0; x < raster.getWidth(); x++) {
                raster.getPixel(x,y,rgba);
                rowSum += (rgba[0] + rgba[1] + rgba[2]) / 3.0;
            }
            brightnessSumPerpendicular[y] = rowSum;
        }
        sinogram[angle+270] = brightnessSumPerpendicular;

        double[] brightnessSumPerpendicularMirrored = new double[raster.getWidth()];
        for(int j = 0; j < brightnessSumPerpendicularMirrored.length; j++)
        {
            brightnessSumPerpendicularMirrored[j] = brightnessSumPerpendicular[brightnessSum.length - j - 1];
        }
        sinogram[angle+90] = brightnessSumPerpendicularMirrored;
        return sinogram;
    }
    /**
     * Creates BufferedImage from sinogram
     *
     * @param controller MainController class for ui updates
     * @return BufferedImage
     */
    public BufferedImage getSinogramAsImage(MainController controller)
    {
        BufferedImage sinogram = new BufferedImage(image.getWidth(), 360, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < 360; y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                double sinogramImageColor = this.sinogram[y][x]/image.getWidth();
                int Pixel=(int)sinogramImageColor<<16 | (int)sinogramImageColor << 8 | (int)sinogramImageColor;
                sinogram.setRGB(x, y,Pixel);
            }
        }
        return sinogram;
    }
}
