package aos.Controller;

import aos.ImageProcessing.ImageHandler;
import aos.ImageProcessing.ImageReconstruction;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import aos.Main;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.function.UnaryOperator;

public class MainController {

    @FXML
    public Button fileOpen, fileSave, reconstructImage;

    @FXML
    public ImageView loadedImageView, reconstructedImageView;

    @FXML
    public TextField projectionsCount, iterationsCount;

    @FXML
    public RadioButton additiveMethod, multiplicativeMethod;

    @FXML
    public ToggleGroup reconstructionMethod;

    @FXML
    public Label reportDialog;

    @FXML
    public CheckBox animateCheckbox, errorCheckbox;

    @FXML
    public ProgressBar progress;

    @FXML
    public ScrollPane reconstructedImageScrollPane;

    private String fileName;
    private BufferedImage loadedImage;
    private ImageHandler imageHandler;

    private Thread reconstructionThread;

    @FXML
    protected void initialize() {
        //-- Controls

        additiveMethod.setUserData("additive");
        multiplicativeMethod.setUserData("multiplicative");

        UnaryOperator<TextFormatter.Change> filter = change -> {
            String text = change.getText();

            if (text.matches("[0-9]*")) {
                return change;
            }

            return null;
        };
        TextFormatter<String> projectionsTextFormatter = new TextFormatter<>(filter);
        projectionsCount.setTextFormatter(projectionsTextFormatter);
        TextFormatter<String> iterationsTextFormatter = new TextFormatter<String>(filter);
        iterationsCount.setTextFormatter(iterationsTextFormatter);
    }

    public void fileChooser() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("Soubory BMP (*.bmp)", "*.BMP");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("Soubory JPG (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("Soubory PNG (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterJPG, extFilterPNG);

        fileChooser.setTitle("Otevřít soubor");
        File loadedFile = fileChooser.showOpenDialog(Main.parentWindow);

        try {
            if (loadedFile != null) {
                loadedImage = ImageIO.read(loadedFile);
                fileName = loadedFile.getName();
                Image image = SwingFXUtils.toFXImage(loadedImage, null);
                if(image.getHeight() != image.getWidth()) {
                    reportMessage("Obrázek nemá poměr stran 1:1", "red");
                    return;
                }

                imageHandler = new ImageHandler(loadedImage);
                Image originalImage = SwingFXUtils.toFXImage(imageHandler.getImage(), null);
                loadedImageView.setImage(originalImage);

                reportMessage("Otevřen obrázek " + fileName, null);
            }
        } catch (IOException ex) {
            reportMessage( "Chyba při načítání souboru.", "red");
        }
    }

    public void saveReconstructedImage() {
        if (reconstructedImageView.getImage() != null) {
            FileChooser fileChooser = new FileChooser();

            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("Soubory PNG (*.png)", "*.png");
            fileChooser.getExtensionFilters().addAll(extFilterPNG);

            fileChooser.setTitle("Uložit rekonstruovaný obraz");
            fileChooser.setInitialFileName("reconstructed-" + fileName);
            File saveFile = fileChooser.showSaveDialog(Main.parentWindow);
            if (saveFile != null) {
                try {
                    System.out.println("Ukládám obrázek");

                    BufferedImage saveImage = SwingFXUtils.fromFXImage(reconstructedImageView.getImage(),null);
                    ImageIO.write( saveImage, "png", saveFile);
                } catch (IOException ex) {
                    reportMessage("Nepodařilo se uložit rekonstruovaný obraz.", "red");
                }
            }
        } else {
            reportMessage("Není rekonstruován žádný obrázek", "red");
        }
    }

    @FXML
    private void openAbout() {
        Label text = new Label();
        text.setText("O programu \n\n CT – Additive ART a Multiplicative ART – simulační program pro ověření vlastností aditivní " +
                "algebraické metody rekonstrukce řezu; pro simulaci metody použijte jednoduché objekty (kruh " +
                "s dírou, trojúhelník, podkova, …) a projekce otočené o definovaný úhel alfa, resp. definovaný " +
                "počet projekcí (základní simulace = 2 projekce).\n");
        text.setMaxWidth(450);
        text.setWrapText(true);
        text.wrapTextProperty();
        BorderPane pane = new BorderPane();
        pane.setCenter(text);
        Scene scene = new Scene(pane);
        pane.setMinHeight(150);
        pane.setMinWidth(500);

        Stage openHelpStage = new Stage();
        openHelpStage.setScene(scene);
        openHelpStage.setMinWidth(520);
        openHelpStage.setMinHeight(180);
        openHelpStage.setTitle("O programu");

        openHelpStage.setOnCloseRequest(
                e -> {
                    e.consume();
                    openHelpStage.close();
                }
        );
        openHelpStage.showAndWait();
    }

    @FXML
    public void showSinogramWindow() {
        if (imageHandler != null) {
            MainController main = this;
            ImageView imageView = new ImageView();
            BorderPane pane = new BorderPane();
            pane.setCenter(imageView);
            Scene scene = new Scene(pane);


            Task task = new Task<Void>() {
                @Override
                public Void call() {
                    if (imageHandler.getSinogram() == null) {
                        imageHandler.generateSinogram(main);
                    }
                    return null;
                }
            };
            task.setOnSucceeded(event -> {
                pane.setMinHeight(360);
                pane.setMinWidth(imageHandler.getImage().getWidth());
                Stage sinogramWindow = new Stage();
                sinogramWindow.setScene(scene);
                sinogramWindow.setMinWidth(imageHandler.getImage().getWidth());
                sinogramWindow.setMinHeight(360);
                sinogramWindow.setTitle("Sinogram");
                imageView.fitWidthProperty().bind(sinogramWindow.widthProperty());
                imageView.fitHeightProperty().bind(sinogramWindow.heightProperty());

                sinogramWindow.setOnCloseRequest(
                        e -> {
                            e.consume();
                            sinogramWindow.close();
                        }
                );
                Image image = SwingFXUtils.toFXImage(imageHandler.getSinogramAsImage(null), null);
                imageView.setImage(image);
                sinogramWindow.showAndWait();
            });
            Thread generateSinogram = new Thread(task);
            generateSinogram.start();
        }
        else
        {
            reportMessage("Není načten výchozí obrázek", null);
        }
    }

    private void reportMessage(String text, String color)
    {
        reportDialog.setText(text);
        if(color == null) {
            color = "black";
        }
        reportDialog.setTextFill(Color.web(color));
    }

    public void reconstructImage()
    {
        if(imageHandler == null)
        {
            reportMessage("Není načten výchozí obrázek", "red");
            return;
        }

        int count = projectionsCount.getText().isEmpty() ? 2 : Integer.parseInt(projectionsCount.getText());
        if (count < 2 || count > 180) {
            reportMessage("Počet projekcí musí být mezi 2 - 180", "red");
            return;
        }
        int iterations = iterationsCount.getText().isEmpty() ? count*3 : Integer.parseInt(iterationsCount.getText());

        MainController main = this;
        if (reconstructionMethod.getSelectedToggle() != null) {

                Task task = new Task<Void>() {
                    @Override
                    public Void call() {
                        ImageReconstruction imageReconstruction = new ImageReconstruction(imageHandler, count, iterations, main);
                        imageReconstruction.setAnimationEnabled(animateCheckbox.isSelected());
                        imageReconstruction.setCheckError(errorCheckbox.isSelected());
                        if (reconstructionMethod.getSelectedToggle().getUserData().toString().equals("additive")) {
                            imageReconstruction.artReconstruction();
                        }
                        else {
                            imageReconstruction.mrtReconstruction();
                        }
                        return null;
                    }
                };

                if(reconstructionThread != null) {
                    if (reconstructionThread.isAlive()) {
                        reconstructionThread.interrupt();
                    }
                }
                reconstructionThread = new Thread(task);
                reconstructionThread.start();
                updateProgressBar(0, true);

        }
    }

    public void updateEstimatedImage(BufferedImage bufferedImage) {
        Platform.runLater(() -> {
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            reconstructedImageView.setImage(image);
            reconstructedImageView.setPreserveRatio(true);
            reconstructedImageView.fitWidthProperty().bind(reconstructedImageScrollPane.widthProperty());
            reconstructedImageView.fitHeightProperty().bind(reconstructedImageScrollPane.heightProperty());
        });
    }

    public void updateStatusText(String text) {
        Platform.runLater(() -> {
            reportMessage(text, null);
        });
    }

    public void updateProgressBar(double percentage, boolean visible) {
        Platform.runLater(() -> {
            progress.setProgress(percentage);
            progress.setVisible(visible);
        });
    }

    @FXML
    public void interruptCurrentReconstruction() {
        Platform.runLater(() -> {
            if (reconstructionThread != null) {
                if (reconstructionThread.isAlive()) {
                    reconstructionThread.interrupt();
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    updateStatusText("Rekonstrukce byla ručně ukončena.");
                    updateProgressBar(0.0, false);
                }
            }
        });
    }
}
