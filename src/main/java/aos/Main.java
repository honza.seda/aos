package aos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage parentWindow;

    @Override
    public void start(Stage mainStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Stage/Main.fxml"));
        Parent root = fxmlLoader.load();
        mainStage.setTitle("Algebraic Reconstruction Technique (ART) simulation");
        mainStage.setScene(new Scene(root, 840, 620));
        mainStage.setMinWidth(820);
        mainStage.setMinHeight(658);
        mainStage.getIcons().add(new Image("/Public/Image/icon.png"));
        mainStage.show();
        Main.parentWindow = mainStage;

        mainStage.setOnCloseRequest(
                e -> {
                    e.consume();
                    System.exit(0);
                }
        );
    }

    public static void main(String[] args) {
        launch(args);
    }
}
