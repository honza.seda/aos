CT – Additive ART a Multiplicative ART
==================================================

Simulační program pro ověření vlastností aditivní
algebraické metody rekonstrukce řezu; pro simulaci metody použijte jednoduché objekty (kruh
s dírou, trojúhelník, podkova, …) a projekce otočené o definovaný úhel , resp. definovaný
počet projekcí (základní simulace = 2 projekce).